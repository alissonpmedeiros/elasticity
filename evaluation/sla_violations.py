class SLA:
    def __init__(self):
        self.cpu_violations = 0
        self.ram_violations = 0
        self.bw_violations = 0

    def check_sla_violation(self, resource, current_value):
        result = 0
        if current_value < 0:
            result = 1
        if resource == "cpu":
            self.cpu_violations+=result
        elif resource == "ram":
            self.ram_violations+=result
        elif resource == "bw":
            self.bw_violations+=result
        else:
            print("ERROR!")
            return -1
        #print("result: {}".format(result))
        #print("current value: {}".format(current_value))

    def calculate_total_violations(self):
        result = self.cpu_violations + self.ram_violations + self.bw_violations
        return result