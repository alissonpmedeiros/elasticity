import re
import matplotlib
import matplotlib.pyplot as plt
from pprint import pprint
import numpy as np
import time
import os
import statistics 
import random

class Services:
    def __init__(self):
        self.total_services = 101
        self.migrated_services = 0
        self.data_migrated=0

        self.cpu_residual = 0
        self.ram_residual = 0
        self.bw_residual  = 0

        self.cpu_total_accepted = 0
        self.cpu_total_rejected = 0

        self.ram_total_accepted = 0
        self.ram_total_rejected = 0
        
        self.bw_total_accepted = 0
        self.bw_total_rejected = 0

        self.cpu_attemps = 0
        self.ram_attemps = 0
        self.bw_attemps = 0

        #self.testbed_option = "kubernetes"
        self.testbed_option = "react"
        self.donations = 0

        self.cpu_time = []
        self.ram_time = []
        self.bw_time  = []

        self.cpu_attempts_time = []
        self.ram_attempts_time = []
        self.bw_attempts_time  = []

        self.total_attempts_time = []
        self.total_events_time   = []
        self.total_time          = []
    
    def get_file_data(self, number_of_clients, directory, resource):
        resource_dict = []
        for client in range(1, number_of_clients):
            file_name = "{}_client{}.txt".format(resource, client)
            try:
                f= open("{}{}".format(directory, file_name),"r").read().split('\n')
                #f = file.read("{}{}".format(directory, file_name).splitlines()
                values = []
                for i in f:
                    if i == '':
                        break
                    values.append(i)
                aux = {'id': client, 'poisson': values}
                resource_dict.append(aux)
            except OSError as err:
                print("OS error: {0}".format(err))
        #print(resource_dict)
        return resource_dict

    def calculate_residual(self, distribution, resource):
        position = 0
        result = 0
        #print(len(distribution))
        #a = input("enter")    
        f= open("./logs/{}_residual_logs.txt".format(resource),"w+")
        while position < 1000:
            for client in distribution:
                values = client.get("poisson")
                aux = re.findall(r'\S+', values[position])
                #print(aux)
                #print(aux[1])
                #a = input("enter:")
                result += float(aux[7])
            #print("Event {}, result = {}".format(position+1, result))
            f.write("{} \n".format(result))
            position+=1
            result=0
        f.close()

    def calculate_total_residual(self):
        number_of_clients = 101
        
        print("calculating CPU residual...")
        resource = "cpu"
        directory = "./poisson/services_workloads/{}/".format(resource)
        cpu_poisson = self.get_file_data(number_of_clients, directory, resource)
        self.calculate_residual(cpu_poisson, "cpu")

        print("\ncalculating RAM residual")
        resource = "ram"
        directory = "./poisson/services_workloads/{}/".format(resource)
        ram_poisson = self.get_file_data(number_of_clients, directory, resource)
        self.calculate_residual(ram_poisson, resource)
        
        print("\ncalculating BANDWIDTH residual")
        resource = "bw"
        directory = "./poisson/services_workloads/{}/".format(resource)
        bw_poisson = self.get_file_data(number_of_clients, directory, resource)
        self.calculate_residual(bw_poisson, resource)
        
    def residual_graphics(self):
        resources = ["cpu", "ram", "bw"]
        directory = "./logs/"
        
        cpu = []
        ram = []
        bw  = []
        auxc = 0
        auxr = 0
        auxb = 0
        for resource in resources:
            try:
                file_name = "{}_residual_logs.txt".format(resource)
                f= open("{}{}".format(directory, file_name),"r").read().split('\n')
                #f = file.read("{}{}".format(directory, file_name).splitlines()
                for i in f:
                    if i == '':
                        break
                    if resource == "cpu":
                        if auxc <= 1000:
                            cpu.append(float(i))
                            auxc+=1
                    elif resource == "ram":
                        if auxr <= 1000:
                            ram.append(float(i))
                            auxr+=1
                    else:
                        if auxb <= 1000:
                            bw.append(float(i))
                            auxb+=1
            except OSError as err:
                print("OS error: {0}".format(err))


        #print("{} : {} : {}".format(cpu[0], ram[0], bw[0]))
        
        x = []
        for i in range(1,1001): x.append(i)
        #print("{}  {}  {}".format(len(cpu), len(ram), len(bw)))
        
        
        plt.bar(x, ram, width=0.8, label='RAM')
        plt.ylabel("RAM Residual Capacity(MB)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/ram_residual.png')
        plt.show()
        plt.cla()

        plt.bar(x, cpu, width=0.8, label='CPU')
        plt.ylabel("CPU Residual Capacity (1 cpu shares = 1024)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/cpu_residual.png')
        plt.show()
        plt.cla()

        plt.bar(x, bw, width=0.8, label='Bandwidth')
        plt.ylabel("Bandwidth Residual Capacity (Mbps)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/bw_residual.png')
        plt.show()
        plt.cla()

    def workload_graphics(self):
        resources = ["cpu", "ram", "bw"]
        directory = "./poisson/services_workloads/"
        
        cpu = []
        ram = []
        bw  = []
        for resource in resources:
            try:
                file_name = "{}_logs.txt".format(resource)
                f= open("{}{}".format(directory, file_name),"r").read().split('\n')
                #f = file.read("{}{}".format(directory, file_name).splitlines()
                for i in f:
                    if i == '':
                        break
                    if resource == "cpu":
                        cpu.append(float(i))
                    elif resource == "ram":
                        ram.append(float(i))
                    else:
                        bw.append(float(i))
            except OSError as err:
                print("OS error: {0}".format(err))


       
        x = []
        for i in range(1,1001): x.append(i)
        
        plt.bar(x, ram, width=0.8, label='RAM')
        plt.ylabel("RAM Workload(MB)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/ram_residual.png')
        plt.show()
        plt.cla()

        plt.bar(x, cpu, width=0.8, label='CPU')
        plt.ylabel("CPU Workload (1 cpu shares = 1024)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/cpu_residual.png')
        plt.show()
        plt.cla()

        plt.bar(x, bw, width=0.8, label='Bandwidth')
        plt.ylabel("Bandwidth Workload (Mbps)")
        plt.xlabel("Number of events")
        plt.legend()
        plt.axhline(y=5, xmin=0.1, xmax=0.8)
        plt.grid()
        plt.tight_layout()
        #plt.savefig('./logs/bw_residual.png')
        plt.show()
        plt.cla()    

    def get_service_workloads(self):
        resources = ["cpu", "ram", "bw"]
        number_of_clients = 101
        workloads = []
        for resource in resources:
            directory = "./poisson/services_workloads/{}/".format(resource)
            result = self.get_file_data(number_of_clients, directory, resource)
            aux = {}
            if resource == "cpu":
                aux = {"cpu": result}
            elif resource== "ram":
                aux = {"ram": result}
            else:
                aux = {"bw": result}
            workloads.append(aux)

        #print(workloads[0]["cpu"][0]["poisson"][0])   
        #acess the data -> resourece_position[0,1,2], resource[cpu,ram,bw], user[0,1,2...100], workload_position[0...1000] 
        return workloads
    
    def service_scheduler(self):
        workloads = self.get_service_workloads()
        number_of_clients = 100
        resources = [[0, "cpu"], [1, "ram"], [2, "bw"]]
        upper_cpu_logs = []
        upper_ram_logs = []
        upper_bw_logs  = []
        lower_cpu_logs = []
        lower_ram_logs = []
        lower_bw_logs  = []
        upper = 0
        lower = 0
        aux_upper = 0
        aux_lower = 0
        
        for i in range(0, 1000):
            upper_cpu_logs.append(0)
            upper_ram_logs.append(0)
            upper_bw_logs.append(0)
            lower_cpu_logs.append(0)
            lower_ram_logs.append(0)
            lower_bw_logs.append(0)
        
        for resource in resources:
            for client in range (0, number_of_clients):
                f= open("./logs/elasticity_attemps/{}/{}_logs.txt".format(resource[1], client),"w+")
                #print("client: {}".format(client))
                aux = workloads[resource[0]][resource[1]][client]["poisson"][0]
                aux = re.findall(r'\S+', aux)
                #print(aux)
                initial_threshold = float(aux[1])
                #print(initial_threshold) 
                #a = input()
                for position in range(0, 1000):
                    workload = workloads[resource[0]][resource[1]][client]["poisson"][position] 
                    #print(workload)
                    aux = re.findall(r'\S+', workload)
                    #print(aux)
                    mrth = float(aux[1])
                    #usage = aux[5]
                    if mrth > initial_threshold:
                        initial_threshold = mrth
                        upper+=1 
                    elif mrth <= initial_threshold * 0.3:
                        initial_threshold =  mrth
                        lower+=1

                    f= open("./logs/elasticity_attemps/{}/{}_logs.txt".format(resource[1], client),"a")
                    message = "upper: {}, lower: {}".format(upper, lower)
                    f.write("{} \n".format(message))
                    f.close()
                    if resource[1] == "cpu":
                        upper_cpu_logs[position] += upper
                        lower_cpu_logs[position] += lower
                    elif resource[1] == "ram":
                        upper_ram_logs[position] += upper
                        lower_ram_logs[position] += lower
                    else:
                        upper_bw_logs[position] += upper
                        lower_bw_logs[position] += lower
                    
                    aux_upper += upper
                    aux_lower += lower
                    upper = 0
                    lower = 0
                    #a = input("")
            f= open("./logs/elasticity_attemps/{}_logs.txt".format(resource[1]),"w+")
            message = "upper: {}, lower: {}".format(aux_upper, aux_lower)
            f.write("{} \n".format(message))
            f.close()
            aux_upper = 0
            aux_lower = 0

        f= open("./logs/elasticity_attemps/cpu_upper_attempts_logs.txt","w+")
        f= open("./logs/elasticity_attemps/ram_upper_attempts_logs.txt","w+")
        f= open("./logs/elasticity_attemps/bw_upper_attempts_logs.txt","w+")

        f= open("./logs/elasticity_attemps/cpu_lower_attempts_logs.txt","w+")
        f= open("./logs/elasticity_attemps/ram_lower_attempts_logs.txt","w+")
        f= open("./logs/elasticity_attemps/bw_lower_attempts_logs.txt","w+")
        
        for i in range(0, 1000):
            cpu_file= open("./logs/elasticity_attemps/cpu_upper_attempts_logs.txt","a")
            cpu_file.write(str(upper_cpu_logs[i]))
            cpu_file.write("\n")
            cpu_file.close()

            cpu_file= open("./logs/elasticity_attemps/cpu_lower_attempts_logs.txt","a")
            cpu_file.write(str(lower_cpu_logs[i]))
            cpu_file.write("\n")
            cpu_file.close()
            
            ram_file= open("./logs/elasticity_attemps/ram_upper_attempts_logs.txt","a")
            ram_file.write(str(upper_ram_logs[i]))
            ram_file.write("\n")
            ram_file.close()

            ram_file= open("./logs/elasticity_attemps/ram_lower_attempts_logs.txt","a")
            ram_file.write(str(lower_ram_logs[i]))
            ram_file.write("\n")
            ram_file.close()
            
            bw_file= open("./logs/elasticity_attemps/bw_upper_attempts_logs.txt","a")
            bw_file.write(str(upper_bw_logs[i]))
            bw_file.write("\n")
            bw_file.close()

            bw_file= open("./logs/elasticity_attemps/bw_lower_attempts_logs.txt","a")
            bw_file.write(str(lower_bw_logs[i]))
            bw_file.write("\n")
            bw_file.close()

        x = []
        for i in range(0, 1000): x.append(i)

        '''
        plt.bar(x, upper_cpu_logs, label="cpu", width=0.8)
        plt.ylabel("CPU Elasticity Attempts (scale up)")
        plt.xlabel("Number of events")
        plt.show()

        plt.bar(x, upper_ram_logs, label="ram", width=0.8)
        plt.ylabel("RAM Elasticity Attempts (scale up)")
        plt.xlabel("Number of events")
        plt.show()

        plt.bar(x, upper_bw_logs, label="wb", width=0.8)
        plt.ylabel("Bandwidth Elasticity Attempts (scale up)")
        plt.xlabel("Number of events")
        plt.show()
        '''
    def calculate_attempts(self, resources, iteration_position, event_time):
        #print("position: {}, accepted: {}, rejected: {}".format(iteration_position, self.total_accepted, self.total_rejected))
        directory = "./poisson/services_workloads/"
        #resources = ["ram"]
        cpu_workload = []
        ram_workload = []
        bw_workload = []
        
        number_of_clients = 100
        workloads = self.get_service_workloads()
        #resources = [[1, "ram"]]
        #resources = [[0, "cpu"], [1, "ram"], [2, "bw"]]

        for resource in resources:
            try:
                file_name = "{}_logs.txt".format(resource[1])
                f= open("{}{}".format(directory, file_name),"r").read().split('\n')
                #f = file.read("{}{}".format(directory, file_name).splitlines()
                for i in f:
                    if i == '':
                        break
                    if resource == "cpu":
                        cpu_workload.append(float(i))
                    elif resource == "ram":
                        ram_workload.append(float(i))
                    else:
                        bw_workload.append(float(i))
            except OSError as err:
                print("OS error: {0}".format(err))

        for resource in resources:
            vod_workload = resource[3]
            is_trigered = False
            accepted = 0
            rejected = 0
            total_time = 0
            donors = self.select_donors(workloads, resource, iteration_position)
            for client in range(0, number_of_clients):
                attempt_directory = "./logs/elasticity_attemps/{}/".format(resource[1])
                file_name = "{}_logs.txt".format(client)
                f= open("{}{}".format(attempt_directory, file_name),"r").read().split('\n')
                aux = f[iteration_position]
                aux = re.findall(r'\S+', aux)
                x = int(aux[1][0])
                if x == 1:
                    is_trigered = True
                else:
                    is_trigered = False

                
                workload = workloads[resource[0]][resource[1]][client]["poisson"][iteration_position]
                aux = re.findall(r'\S+', workload)
                mrth = float(aux[1])
                available_resource = resource[2]
                resource_demand = (vod_workload + mrth) - available_resource
                if (vod_workload + mrth) <= available_resource:
                    vod_workload += mrth
                    #print("iteration: {}".format(vod_workload))
                    if is_trigered:
                        accepted+=1
                        total_time += self.execution_time(resource[1], mrth) 
                else:
                    if is_trigered:
                        #print("available resource: {}".format(available_resource))
                        #print("demand:{}".format(resource_demand))
                        #print("service: {}".format(aux))
                        #print(type(aux[1]))
                        #a = input()
                        
                        if self.testbed_option == "react":
                            result, solidarity_time = self.solidarity(donors, workloads, resource, iteration_position, resource_demand, client)
                            if result:
                                accepted+=1
                                self.donations+=1
                                total_time += solidarity_time
                            else:
                                rejected+=1
                        else:
                            rejected+=1
                #print("accepted: {} rejected: {}".format(accepted, rejected))
            
            
            '''
            residual_sum = self.residual_sum(donors)
            self.plot_new_residual(resource, residual_sum)
            f = open("./logs/{}/{}_attempts.txt".format(self.testbed_option, resource[1]),"a")
            message = "accepted: {} rejected: {}".format(accepted, rejected)
            f.write(message)
            f.write("\n")
            f.close()
            #print("video ram workload: {}".format(video_ram))
            #print("services ram workload: {}".format(round((services_workload - video_ram), 2)))
            #print("total ram workload: {}".format(round(services_workload, 2)))
            '''
            if rejected == 0:
                rejected = 0.001
            if resource[1] == "cpu": 
                '''
                attemps = event_time * rejected
                self.cpu_attemps += round(attemps, 0)
                f = open("./logs/{}/denied_attempts/{}_denied_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "{}".format(self.cpu_attemps)
                f.write(message)
                f.close()
                
                self.cpu_total_accepted += accepted
                self.cpu_total_rejected += rejected
                f = open("./logs/{}/{}_final_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "accepted: {} rejected: {}".format(self.cpu_total_accepted, self.cpu_total_rejected)
                f.write(message)
                f.write("\n")
                f.close()
                '''
                self.cpu_attempts_time.append(rejected * (total_time/100))
                self.cpu_time.append(total_time/100) 

            elif resource[1] == "ram": 
                '''
                attemps = event_time * rejected
                self.ram_attemps += round(attemps, 0)
                f = open("./logs/{}/denied_attempts/{}_denied_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "{}".format(self.ram_attemps)
                f.write(message)
                f.close()
                
                self.ram_total_accepted += accepted
                self.ram_total_rejected += rejected
                f = open("./logs/{}/{}_final_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "accepted: {} rejected: {}".format(self.ram_total_accepted, self.ram_total_rejected)
                f.write(message)
                f.write("\n")
                f.close()
                '''
                self.ram_attempts_time.append(rejected * (total_time/100))
                self.ram_time.append(total_time/100) 
            else: 
                '''
                attemps = event_time * rejected
                self.bw_attemps += round(attemps, 0)
                f = open("./logs/{}/denied_attempts/{}_denied_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "{}".format(self.bw_attemps)
                f.write(message)
                f.close()
                
                self.bw_total_accepted += accepted
                self.bw_total_rejected += rejected
                f = open("./logs/{}/{}_final_attempts.txt".format(self.testbed_option, resource[1]),"+w")
                message = "accepted: {} rejected: {}".format(self.bw_total_accepted, self.bw_total_rejected)
                f.write(message)
                f.write("\n")
                f.close()
                '''
                self.bw_attempts_time.append(rejected * (total_time/100))
                self.bw_time.append(total_time/100) 
            
        if iteration_position == 999:
            self.calculate_execution_time()

    def takeResidual(self, donors):
        #print(type(donors[7]))
        #a = input()
        return float(donors[7])

    def select_recipient(self, service, new_mrth, new_residual):
        service[1] = new_mrth
        service[7] = new_residual
    
    def select_donors(self, workloads, resource, iteration_position):
        number_of_clients = 100
        donors = []
        for service in range(0, number_of_clients):
            workload = workloads[resource[0]][resource[1]][service]["poisson"][iteration_position]
            aux = re.findall(r'\S+', workload)
            donors.append(aux)
        donors.sort(key=self.takeResidual, reverse=True)
        return donors

    def residual_sum(self, workload):
        result = 0
        for w in workload:
            result += float(w[7])
        return result

    def plot_new_residual(self, resource_type, residual_sum):
        f= open("./logs/react/donation_logs/new_{}_residual.txt".format(resource_type[1]),"a")
        f.write(str(residual_sum))
        f.write("\n")
        f.close()
        
    def solidarity(self, donors, workloads, resource, iteration_position, resource_demand, recipient_position):
        #donors = self.select_donors(workloads, resource, iteration_position)
        total_time = 0
        donations = 0
        for donor in donors:
            mrth     = float(donor[1])
            cth      = float(donor[3])
            usage    = float(donor[5])
            residual = float(donor[7])
            if usage < cth:
                donations += mrth - cth
                #print("resource demand: {}".format(resource_demand))
                #print("previous residual: {}".format(residual))
                residual  -= mrth - cth
                #print("new residual: {}".format(residual))
                #a = input()
                mrth = cth
                donor[1] = str(mrth)
                total_time += self.execution_time(resource[1], mrth)
                donor[7] = str(residual)
                if donations >= resource_demand:
                    break

        if donations >= resource_demand:
            #print("demand:{}, donations: {}".format(resource_demand, donations))
            #a = input()
            #pprint(donors[recipient_position])
            #a = input()
            donors[recipient_position][1] = str(donations)
            total_time += self.execution_time(resource[1], donations)
            #residual_sum = self.residual_sum(donors)
            #self.plot_new_residual(resource, residual_sum)                
            return True, total_time
        else:
            #print("demand:{}, donations: {}".format(resource_demand, donations))
            #a = input()
            donors[recipient_position][1] = str(donations)
            #residual_sum = self.residual_sum(donors)
            #self.plot_new_residual(resource, residual_sum)
            return False, total_time 


    def execution_time(self, resource, elasticity_value):   
        
        if resource == "cpu":
            #start_time = time.time()
            #os.system("docker update --cpu-shares={} vlc".format(int(elasticity_value)))
            #total_time = time.time() - start_time
            total_time = round(random.uniform(0.02, 0.09), 3)
            #self.cpu_time.append(total_time)
            return total_time
        elif resource == "ram":
            #start_time = time.time()
            #os.system("docker update -m={}m vlc".format(elasticity_value))
            #total_time = time.time() - start_time
            total_time = round(random.uniform(0.04, 0.10), 3)
            #self.ram_time.append(total_time)
            return total_time
        else:
            #start_time = time.time()
            #os.system("sudo ovs-vsctl set interface eth0 ingress_policing_rate={}".format(int(elasticity_value)*1000))
            #os.system("sudo ovs-vsctl set interface eth0 ingress_policing_burst=0")
            #total_time = time.time() - start_time
            total_time = round(random.uniform(0.01, 0.05), 3)
            total_time += round(random.uniform(0.01, 0.05), 3)
            #self.bw_time.append(total_time)
            return total_time

    def calculate_execution_time(self):
        
        for i in range(0, 1000):
            events   = self.cpu_time[i] + self.ram_time[i] + self.bw_time[i] 
            attempts = self.cpu_attempts_time[i] + self.ram_attempts_time[i] + self.bw_attempts_time[i] 
            self.total_events_time.append(events)
            self.total_attempts_time.append(attempts)
            self.total_time.append(events + attempts)
        
        print("\n##################   {} elasticity events  ########################".format(self.testbed_option))
        print("Events execution time median: {}".format(statistics.median(self.total_events_time)))
        print("Events execution time standard deviation: {}".format(statistics.stdev(self.total_events_time)))
        print("\n####################################################################\n\n")
    
        print("\n##################   {} elasticity attempts  ########################".format(self.testbed_option))
        print("Attempts execution time median: {}".format(statistics.median(self.total_attempts_time)))
        print("Attempts execution time standard deviation: {}".format(statistics.stdev(self.total_attempts_time)))
        print("\n####################################################################\n\n")

        print("\n########################   {} total time  ##########################".format(self.testbed_option))
        print("Total execution time median: {}".format(statistics.median(self.total_time)))
        print("Total execution time standard deviation: {}".format(statistics.stdev(self.total_time)))
        print("\n####################################################################\n\n")
    
    
        

        

        f= open("./logs/{}/time/cpu_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/cpu_time.txt".format(self.testbed_option),"a")
        for t in self.cpu_time:
            f.write(str(t))
            f.write("\n")
        f.close()

        f= open("./logs/{}/time/ram_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/ram_time.txt".format(self.testbed_option),"a")
        for t in self.ram_time:
            f.write(str(t))
            f.write("\n")
        f.close()

        f= open("./logs/{}/time/bw_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/bw_time.txt".format(self.testbed_option),"a")
        for t in self.bw_time:
            f.write(str(t))
            f.write("\n")
        f.close()

        f= open("./logs/{}/time/cpu_attempts_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/cpu_attempts_time.txt".format(self.testbed_option),"a")
        for t in self.cpu_attempts_time:
            f.write(str(t))
            f.write("\n")
        f.close()

        f= open("./logs/{}/time/ram_attempts_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/ram_attempts_time.txt".format(self.testbed_option),"a")
        for t in self.ram_attempts_time:
            f.write(str(t))
            f.write("\n")
        f.close()

        f= open("./logs/{}/time/bw_attempts_time.txt".format(self.testbed_option),"+w")
        f= open("./logs/{}/time/bw_attempts_time.txt".format(self.testbed_option),"a")
        for t in self.bw_attempts_time:
            f.write(str(t))
            f.write("\n")
        f.close()