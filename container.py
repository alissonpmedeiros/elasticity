#!/usr/bin/python3

class Container:
    def __init__(self, name):

        self.name = name

        ## CPU variables ##
        self.sts_total_usage = 0
        self.sts_system_cpu_usage =0
        self.previous_cpu_total_usage = 0
        self.previous_system_cpu_usage = 0
        self.host_cores = 0
        self.nano_cpus = 0
        self.cpu_cores_usage = 0
        self.cpu_usage_percentage = 0
        self.cpu_stats_usage = 0

        ## Memory variables ##
        self.memory_usage = 0	
        self.memory_cache = 0
        self.limit_memory = 0
        self.memory_mebibytes = 0 	#in Mebibyte
        self.memory_usage_percentage = 0
        
        ## variables to provide downgrade elasticity ##
        self.cpu_cont_lower = 0
        self.memory_cont_lower = 0

        self.ip = ""

    ## RAM Functions ##
    def calculate_memory(self):     
        #1 Mebibyte = 1.048576 Megabyte 
        memory_percent = ((self.memory_usage - self.memory_cache ) / self.limit_memory) * 100
        self.memory_mebibytes =  round((self.limit_memory / 1048576), 2)
        self.memory_usage_percentage = round(memory_percent, 2)


    ## CPU Functions ##
    def get_container_cpu(self):
        cpuDelta = self.sts_total_usage - self.previous_cpu_total_usage
        systemDelta = self.sts_system_cpu_usage - self.previous_system_cpu_usage
        cpuPercent = (cpuDelta / systemDelta) * self.host_cores * 100
        #print("CPU Percent:", round(cpuPercent, 2))
        #print("Docker CPU:", round(cpuPercent, 2))
        self.cpu_stats_usage = round(cpuPercent, 2)

    def cores_usage(self):
        self. cpu_cores_usage = self.nano_cpus / 1000000000
     

    def calculate_cpu(self):
        self.cores_usage()
        self.get_container_cpu()
        #print("*calculate cpu function*")
        result = self.cpu_stats_usage / self.cpu_cores_usage
        self.cpu_usage_percentage = round(result, 2)