#!/usr/bin/python3
import docker



class DockerMonitoring:
    def __init__(self):
        self.client_lowlevel = docker.APIClient(base_url='unix://var/run/docker.sock')    
    
    def get_container_metrics(self, container):
        #print("*** Container Metrics ***")
        container_stats = self.client_lowlevel.stats(container=container.name,decode=False, stream=False)
        #print (container_stats)
        #sys.exit("Error message")

        for key, value in container_stats.items():
            if(key == "cpu_stats"):
                container.sts_total_usage = container_stats[key]['cpu_usage']['total_usage']
                container.sts_system_cpu_usage = container_stats[key]['system_cpu_usage']
            elif(key == "precpu_stats"):
                container.previous_cpu_total_usage = container_stats[key]['cpu_usage']['total_usage']
                container.previous_system_cpu_usage = container_stats[key]['system_cpu_usage']
                container.host_cores = container_stats[key]['online_cpus']
                #print("host cores:",container.host_cores)
            elif(key == "memory_stats"):
                container.limit_memory = container_stats[key]['limit']
                container.memory_usage = container_stats[key]['usage']
                container.memory_cache = container_stats[key]['stats']['cache']

        container_inspect = self.client_lowlevel.inspect_container(container.name)
        #print(container_inspect)
        for key, value in container_inspect.items():
            if key == "HostConfig":
                container.nano_cpus = container_inspect[key]["NanoCpus"]
                
            #if key == "NetworkSettings":
                #container.ip = container_inspect[key]["Networks"]["bridge"]["IPAddress"]
                
        container.calculate_memory()
        container.calculate_cpu()
        