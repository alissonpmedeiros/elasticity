#!/usr/bin/python3
import os
import sys
host_memory = 4096
#host_memory = 600

## Elasticity Functions ##

def cpu_elasticity_upper_bound(container):
	if container.cpu_cores_usage == container.host_cores:
		return

	if container.cpu_cores_usage + (container.cpu_cores_usage * 0.3) <= container.host_cores:
		container.cpu_cores_usage += container.cpu_cores_usage * 0.3
	else:
		container.cpu_cores_usage = container.host_cores

	message = "docker update --cpus=" + str(round(container.cpu_cores_usage, 2)) + " " + container.name
	print("message:", message)
	os.system(message)
	

def cpu_elasticity_lower_bound(container):
	if container.cpu_cores_usage < 30:
		container.cpu_cont_lower +=1
	else:
		container.cpu_cont_lower = 0

	if container.cpu_cont_lower >= 5:
		if (container.cpu_cores_usage  - (container.cpu_cores_usage* 0.2)) > 0.2:
			container.cpu_cores_usage -= container.cpu_cores_usage * 0.2
			message = "docker update --cpus=" + str(round(container.cpu_cores_usage, 2)) + " " + container.name
			print(message)
			os.system(message)
		container.cpu_cont_lower = 0


def memory_elasticity_upper_bound(container):
	if container.memory_mebibytes == host_memory:
		return

	
	if container.memory_mebibytes + (container.memory_mebibytes * 0.3) < host_memory: 
		container.memory_mebibytes += round((container.memory_mebibytes * 0.3), 2)
	else:
		container.memory_mebibytes = host_memory
	message = "docker update -m=" + str(container.memory_mebibytes) + "m " + container.name
	print("message:", message)
	os.system(message)
	

def memory_elasticity_lower_bound(container):
	if container.memory_usage_percentage < 30:
		container.memory_cont_lower +=1
	else:
		container.memory_cont_lower = 0

	if container.memory_cont_lower >=5:
		if (container.memory_mebibytes - (container.memory_mebibytes * 0.2)) > 30:
			container.memory_mebibytes -= round((container.memory_mebibytes * 0.2), 2)
			message = "docker update -m=" + str(container.memory_mebibytes) + "m " + container.name
			print(message)
			os.system(message)
		container.memory_cont_lower = 0
		


def elasticity_orchestrator(container):
	print("Container: {}".format(container.name))
	print("Host: {} CPU CORES, and {} GiB".format(container.host_cores, host_memory))
	print("Memory Limit:", container.memory_mebibytes, "MiB")
	print("Memory Usage percent:", container.memory_usage_percentage, "%")
	print("CPU Limit: {}".format(container.cpu_cores_usage))
	print("CPU usage percent:", container.cpu_usage_percentage, "%")
	
	if container.cpu_usage_percentage > 70:
		cpu_elasticity_upper_bound(container)
	else:
		cpu_elasticity_lower_bound(container)
	
	if container.memory_usage_percentage > 70:
		memory_elasticity_upper_bound(container)
	else:
		memory_elasticity_lower_bound(container)
	





