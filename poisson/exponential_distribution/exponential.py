import matplotlib.pyplot as plt
import numpy as np


s = np.random.exponential(15, size=100)
print(s)
plt.plot(s, label="test")
plt.ylabel("Time (seconds)")
plt.xlabel("Number of events")
plt.show()

count, bins, ignored = plt.hist(s, 14)
plt.ylabel("Probability density")
plt.xlabel("Number of events' frequency")
plt.show() 


f= open("exponential.txt","w+")
for i in s:
    f.write("%d\r\n" % (i))
f.close()
