
import re
from pprint import pprint
import matplotlib.pyplot as plt
import random
import numpy as np

def get_file_data(number_of_clients, resource):
    resource_dict = []
    for client in range(1, number_of_clients):
        file_name = "{}_client{}.txt".format(resource, client)
        directory = "./{}/".format(resource)
        try:
            f= open("{}{}".format(directory, file_name),"r").read().split('\n')
            #f = file.read("{}{}".format(directory, file_name).splitlines()
            values = []
            for i in f:
                if i == '':
                    break
                values.append(i)
            aux = {'id': client, 'poisson': values}
            resource_dict.append(aux)
        except OSError as err:
            print("OS error: {0}".format(err))
    #print(resource_dict)
    return resource_dict

def calculate_resource_usage(distribution, resource):
    position = 0
    result = 0
    #print(len(distribution))
    #a = input("enter")    
    f= open("./{}_logs.txt".format(resource),"w+")
    while position < 1000:
        for client in distribution:
            values = client.get("poisson")
            aux = re.findall(r'\S+', values[position])
            #print(aux)
            #print(aux[1])
            #a = input("enter:")
            result += float(aux[1])
        #print("Event {}, result = {}".format(position+1, result))
        f.write("{} \n".format(result))
        position+=1
        result=0
    f.close()

def generate_workload(resource, ratio, total_clients):
    for client in range(1,total_clients):
        workload = np.random.rayleigh(ratio, size=1000)
        f= open("./{}/{}_client{}.txt".format(resource, resource, client),"w+")
        for i in workload:
            mrth = mrth = round(i, 2)
            cth = round(random.uniform(mrth * 0.7, mrth * 0.9), 2)
            usage = round(random.uniform(mrth * 0.5, mrth), 2)
            residual = round(mrth - usage, 2)
            f.write("mrth: {} cth: {} usage: {} residual: {}\n".format(mrth, cth, usage, residual))
        f.close()


if __name__ == "__main__":
    number_of_clients = 101

    print("generating services cpu workload")
    generate_workload("cpu", 77, number_of_clients)
    print("generating services ram workload")
    generate_workload("ram", 160, number_of_clients)
    print("generating services bw workload")
    generate_workload("bw", 45, number_of_clients)
    
    print("CPU workload checking")
    cpu_poisson = get_file_data(number_of_clients, "cpu")
    calculate_resource_usage(cpu_poisson, "cpu")

    print("\nRAM workload checking")
    ram_poisson = get_file_data(number_of_clients, "ram")
    calculate_resource_usage(ram_poisson, "ram")

    print("\nBANDWIDTH workload checking")
    bw_poisson = get_file_data(number_of_clients, "bw")
    calculate_resource_usage(bw_poisson, "bw")
