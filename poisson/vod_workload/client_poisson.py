import matplotlib.pyplot as plt
import numpy as np


s = np.random.poisson(lam=20, size=100)
print(s)
plt.plot(s, label="test")
plt.ylabel("Number of clients")
plt.xlabel("Iterations")
plt.show()

count, bins, ignored = plt.hist(s, 14)
plt.ylabel("Probability density")
plt.xlabel("Number of clients' frequency")
plt.show() 

f= open("logs.txt","w+")
for i in s:
    f.write("%d\r\n" % (i))
f.close()