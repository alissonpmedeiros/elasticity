#!/usr/bin/python3
import os
import sys
import time
import json
import subprocess
import docker
from pprint import pprint
import container, monitoring, elasticity
from evaluation import sla_violations, services

## VARIABLES ##

## Host variables
available_cpus=8192 #in cgroups share mode, where 1 cpu = 1024 cpus shares
available_ram=16384  #16GB ram
available_bw=5000 #5GBPS


## System variables ##

clear = lambda: os.system('clear')
client = docker.from_env()





def get_service_workload(directory, file_name):
    results = []
    try:
        f = open("{}{}".format(directory, file_name),"r").read().split('\n')
        for result in f:
            if result =='':
                break
            results.append(float(result))
        
    except OSError as err:
        print("OS error: {0}".format(err))
    #print(results)
    return results




if __name__ == "__main__":
    new_container = container.Container("vlc")
    monitoring = monitoring.DockerMonitoring()
    #SLA = sla_violations.SLA()
    service = services.Services()
    #service.workload_graphics()
    #service.service_scheduler()
    #service.calculate_total_residual()
    #service.residual_graphics()
    '''
    f = open("./logs/{}/cpu_attempts.txt".format(service.testbed_option),"+w")
    f = open("./logs/{}/ram_attempts.txt".format(service.testbed_option),"+w")
    f = open("./logs/{}/bw_attempts.txt".format(service.testbed_option),"+w")
    
    if service.testbed_option == "react":
        f = open("./logs/{}/donation_logs/new_cpu_residual.txt".format(service.testbed_option),"+w")    
        f = open("./logs/{}/donation_logs/new_ram_residual.txt".format(service.testbed_option),"+w")     
        f = open("./logs/{}/donation_logs/new_bw_residual.txt".format(service.testbed_option),"+w")         
    '''
    cpu_workload = get_service_workload("./poisson/vod_workload/", "vod_cpu_workload.txt")
    ram_workload = get_service_workload("./poisson/vod_workload/", "vod_ram_workload.txt")
    bw_workload = get_service_workload("./poisson/vod_workload/", "vod_bw_workload.txt")
    event_time = get_service_workload("./poisson/exponential_distribution/", "exponential.txt")



    a = input()
    for i in range(0, 1000):
        print(i)
        #monitoring.get_container_metrics(new_container)
        #available_ram = ram - (log + new_container.memory_mebibytes )
        #print("available RAM: {}".format(available_ram))
        #SLA.check_sla_violation("ram", available_ram)
        #print("SLA VIOLATIONS: {}".format(SLA.ram_violations))
        #elasticity.elasticity_orchestrator(new_container)
        #service.calculate_attempts(ram, cont, new_container.memory_mebibytes)
        vod_cpu =cpu_workload[i]
        vod_memory = ram_workload[i]
        vod_bw = bw_workload[i]
        #print(vod_memory)
        resources = [[0, "cpu", available_cpus, vod_cpu], [1, "ram", available_ram, vod_memory], [2, "bw", available_bw, vod_bw]]
        service.calculate_attempts(resources, i, event_time[i])
        #print("ITERATION TIME: {}".format(time.clock() - start_time))
        #print("\n")
        
       
    #report()